extern crate walkdir;
extern crate regex;

use walkdir::WalkDir;
use regex::Regex;
use std::env;
use std::fs;
use std::path::Path;
use std::io::Read;

fn main() {
    let re = Regex::new(r"<\|(.+)\|>").unwrap();
    
    let args: Vec<String> = env::args().collect();
    let path = if args.len() == 2 {
        (&args[1]).into()
    } else {
        ".".to_string()
    };
    
    for e in WalkDir::new(path).into_iter().filter_map(|e| e.ok()) {
        if e.metadata().unwrap().is_file() {
            let path = Path::new(e.path());
            let mut file = fs::File::open(path).unwrap();
            let mut cont = String::new();
            file.read_to_string(&mut cont).expect("File I/O Error");
            let dir = path.parent().unwrap().to_str().unwrap();
            for cap in re.captures_iter(&cont.clone()) {
                let mut incl = String::new();
                fs::File::open(Path::new(&format!("{}/{}", &dir, &cap[1]))).unwrap().read_to_string(&mut incl);
                cont = cont.replace(&cap[0], &incl);
            }
            fs::write(path, &cont.as_bytes()).expect("File I/O Error")
        }
    }
}
